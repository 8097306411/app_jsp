package com.sprinboot.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HttpController {
	
	private static final Logger logger = LoggerFactory.getLogger(HttpController.class);
	
//	 @RequestMapping(value = "/", method = RequestMethod.GET)
//     public String hello(ModelMap model) {
//         model.addAttribute("msg", "Hello Amit !!");
//         //return "helloWorld";
//         logger.info("/ method calling");
//         return "welcome";
//     }
//	 
	 @RequestMapping("/")
		public String welcome(Map<String, Object> model) {
			model.put("message", "Hello Amit !!");
			logger.info("/ method calling");
			return "welcome";
		}
      
     @RequestMapping(value = "/hello/{msg}", method = RequestMethod.GET)
     public String displayMessage(@PathVariable String msg, ModelMap model) {
         model.addAttribute("msg", "Hello "+msg+" !!!");
         //return "helloWorld";
         return "welcome";
     }

}
